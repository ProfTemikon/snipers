$(document).ready(function(){

  $(function(){
    $('.header-search-btn').click(function(e){
      e.preventDefault();

      $(this).toggleClass(' header-search-btn_active');
    });
  });

  $(function(){
    $('.owl-carousel').owlCarousel({
      items: 1
    });
  });

  $(function(){
    var playAudio = true;
    $('.main-tracks__capt').click(function(){

      var track = $(this).siblings('.main-tracks__audio')[0];
      if (playAudio == true) {
        track.play();
        playAudio = false;
      } else {
        track.pause();
        playAudio = true;
      };
    });
  });

});